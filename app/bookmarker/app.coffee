app = angular.module 'Bookmarker', ['ui.router', 'ui.bootstrap']


app.config ['$httpProvider', '$stateProvider', '$urlRouterProvider', '$interpolateProvider', ($httpProvider, $stateProvider, $urlRouterProvider, $interpolateProvider) ->

        # Required for csrf checks in the backend.
        $httpProvider.defaults.xsrfCookieName = 'csrftoken'
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
        delete $httpProvider.defaults.headers.common['X-Requested-With']
        $httpProvider.defaults.useXDomain = true

        return
]

app.config require './app.routes.coffee'

app.controller 'BookmarkListCtrl', require './controllers/bookmark_list.coffee'
app.controller 'CreateBookmarkCtrl', require './controllers/create_bookmark.coffee'
app.controller 'UpdateBookmarkCtrl', require './controllers/update_bookmark.coffee'

app.service 'BookmarkService', require './services/bookmark.coffee'

