module.exports = ['$scope', '$state', '$location', 'BookmarkService', ($scope, $state, $location, BookmarkService) ->
	
	$scope.app = {}
	$scope.get_params = $location.search()

	# Redirects to the page from new bookmark can be added
	$scope.goToCreate = () ->
		$state.go('bookmarks_create')

	# Redirects to the edit page of the given bookmark
	$scope.editBookmark = (bookmark) ->
		$state.go 'bookmarks_update', {'id': bookmark.id}

	$scope.filterBookmarks = (keyword) ->
		data = {'search': keyword}
		BookmarkService.all(data)
			.success (response) ->
				$scope.app.bookmarks = response

	fetchBookmarks = () ->
		# If there is a search get parameter
		# filter the bookmark list based on it.
		if $scope.get_params.search?
			data = {'search': $scope.get_params['search']} 
		else
			data = {}
		BookmarkService.all(data)
			.success (response) ->
				$scope.app.bookmarks = response

	$scope.deleteBookmark = (id) ->
		BookmarkService.delete(id)
			.success (response) ->
				# Calling the api again to update the list.
				fetchBookmarks()

	fetchBookmarks()

	@
	]
