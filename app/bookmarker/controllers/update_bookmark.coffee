module.exports = ['$scope', '$state', '$stateParams', 'BookmarkService', ($scope, $state, $stateParams, BookmarkService) ->
	
	$scope.form_data = {}
	$scope.errors = {}

	fetchBookmark = (id) ->
		BookmarkService.fetch(id)
			.success (response) ->
				$scope.form_data = response

	$scope.updateBookmark = (form_data) ->
		data = {'name': form_data.name, 'url': form_data.url}
		BookmarkService.update(form_data.id, data)
			.success  ->
				$state.go('bookmarks')
			.error (response) ->
				$scope.errors = response
	
	fetchBookmark($stateParams.id)

	@
	]
