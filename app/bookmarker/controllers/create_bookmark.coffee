module.exports = ['$scope', '$state', 'BookmarkService', ($scope, $state, BookmarkService) ->
	$scope.form_data = {}
	$scope.errors = {}
	
	$scope.createBookmark = (data) ->
		BookmarkService.create(data)
			.success (response) ->
				$state.go 'bookmarks'
			.error (response) ->
				$scope.errors = response
	
	@
	]
