module.exports = ['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
        $stateProvider
                
                .state 'bookmarks', {
                        url: '/bookmarks',
                        templateUrl: '/static/templates/bookmarks.html',
                        controller: 'BookmarkListCtrl',
                        data : requireLogin: false,
                        }
                .state 'bookmarks_create', {
                        url: '/create',
                        templateUrl: '/static/templates/bookmarks.create.html',
                        controller: 'CreateBookmarkCtrl',
                        }
                .state 'bookmarks_update', {
                        templateUrl: '/static/templates/bookmarks.update.html',
                        controller: 'UpdateBookmarkCtrl',
                        url: '/update/:id',
                        }

        $urlRouterProvider.otherwise("/bookmarks");
                
                        
        return
        ]
