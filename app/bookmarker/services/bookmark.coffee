module.exports = ['$http', ($http) ->
	bookmarkUrl = ->
		'/bookmarks/api/v1/'
	@.all = (params) ->
		$http.get bookmarkUrl(), params:params
	@.fetch = (id) ->
		$http.get bookmarkUrl()+id
	@.delete = (id) ->
		$http.delete bookmarkUrl()+id

	@.update = (id, data) ->
		$http.put bookmarkUrl()+id+'/', data

	@.create = (data) ->
		$http.post bookmarkUrl(), data

	
	@
	]
