module.exports = {
context: __dirname + '/app/',
    entry: './main.js',
    output: {
	path: 'build/',
	filename: 'bundle.js'
    },
    module: {
	loaders: [
	    { test: /\.coffee$/, loader: "coffee-loader" }
	]
    },
    resolve: {
	extensions: ["", ".coffee", ".js"]
    }
};
