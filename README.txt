To run the demo
===============
* Create a virtualenv and install the dependencies
	$mkvirtualenv resello
	$pip install -r requirements.txt
	$python manage.py migrate

* Run the server
	$python manage.py runserver

* Now open http://localhost:8000/static/index.html in browser

* Build directory is added as one of the static file directories, so django dev server
  can serve the angular templates and index.html
