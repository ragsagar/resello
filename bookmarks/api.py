from django.conf.urls import url, include

from rest_framework.urlpatterns import format_suffix_patterns

from .views import BookmarkListAPIView, BookmarkDetailAPIView


urlpatterns = [
        url(r'^$', BookmarkListAPIView.as_view()),
        url(r'^(?P<pk>[0-9]+)/$', BookmarkDetailAPIView.as_view())
        ]

urlpatterns = format_suffix_patterns(urlpatterns)
