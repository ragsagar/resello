from rest_framework import serializers

from .models import Bookmark


class BookmarkSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bookmark
        fields = ('created', 'url', 'name', 'order', 'id')



