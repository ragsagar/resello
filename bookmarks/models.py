from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class Bookmark(models.Model):
    created = models.DateTimeField(default=timezone.now, editable=False)
    url = models.URLField()
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-order', 'created']
