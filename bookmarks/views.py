from django.shortcuts import render

from rest_framework import generics

from .models import Bookmark
from .serializers import BookmarkSerializer


class BookmarkListAPIView(generics.ListCreateAPIView):
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer

    def get_queryset(self):
        """ Overiding to enable search on name field. """
        qs = super(BookmarkListAPIView, self).get_queryset()
        search_word = self.request.GET.get('search')
        # Whole queryset will be returned if there no search word.
        if search_word:
            qs = qs.filter(name__icontains=search_word)
        return qs


class BookmarkDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer
